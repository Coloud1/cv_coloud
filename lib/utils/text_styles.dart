import 'package:flutter/material.dart';

const TextStyle INTER_THIN = TextStyle(fontWeight: FontWeight.w300);

const TextStyle INTER_REGULAR = TextStyle(fontWeight: FontWeight.w400);

const TextStyle INTER_MEDIUM = TextStyle(fontWeight: FontWeight.w500);

const TextStyle INTER_SEMIBOLD = TextStyle(fontWeight: FontWeight.w600);

const TextStyle INTER_BOLD = TextStyle(fontWeight: FontWeight.w700);