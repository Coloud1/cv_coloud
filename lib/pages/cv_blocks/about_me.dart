import 'package:flutter/material.dart';

class AboutMe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("\nI am Junior Flutter developer. I am currently working on a project for auto auctions. "
            "In my portfolio there are 2 released projects in the AppStore and Playmarket, as well "
            "as 1 project that should be released in the near future. "
            "Able to work alone, as well as in a team with strong developers.\n"),

      ],
    );
  }
}
