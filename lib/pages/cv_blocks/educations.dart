import 'package:flutter/material.dart';

class Educations extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,

      children: [
        Text("\n2015-2019 - Central Ukrainian National Technical University, Systems Engineering, Bachelor\n"),
        Text("\n2019-2020 - Central Ukrainian National Technical University, Automation and computer-integrated technologies, Master\n")

      ],
    );
  }
}
