import 'package:flutter/material.dart';

class FullQr extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset('assets/images/qr-code.gif'),
    );
  }
}
